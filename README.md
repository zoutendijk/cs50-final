# Learn M3 | Final project CS50
Created by Jan Zoutendijk

## About M3
Learn M3 is my final project for CS50. I really wanted to try something with machine learning, so I chose to make a simple image recognition interface. M3 is made with Tensorflow, an open source machine learning platform.

Special thanks goes out to the Youtube content creator sentdex for making a great Youtube series on how to make a simple image recognition machine learning app with Tensorflow. Also thanks to Freepik for the free images. And last but not least for Harvard university for making this great course available for free!

## Summary
- Python web app with Flask
- Self learning neural network that takes a dataset of images and is able to predict new input
- Input is created by user
- Input set can be saved and loaded within app

## Nice to haves
- Check with check50 and W3C
- Use .env config file?
- Delete model option
- Use external file system for images
- Add user space limitation
- Fix models and featues sanitizing so that names can still be used. Probably by using DB.

## Server   
This app runs on a free Heroku plan. This means that the first time you visit this page it might be slower than usual. Also currently all models will be deleted after each push :(

## Sanitizing
All models and features get sanitized before creating a directory so that the directories can be used to get the relevant data. This does rename the model and feature names.

## Model folder structure
- models/userid/modelname/
- models/userid/modelname/model.model
- models/userid/modelname/images1/
- models/userid/modelname/images2/
- models/userid/modelname/images3/
etc..