import os
import requests
import urllib.parse
import re

from flask import redirect, render_template, request, session
from functools import wraps


def apology(message, code=400):
    return render_template("apology.html", top=code, bottom=message), code


def login_required(f):
    """
    Decorate routes to require login.

    http://flask.pocoo.org/docs/1.0/patterns/viewdecorators/
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get("user_id") is None:
            return redirect("/login")
        return f(*args, **kwargs)
    return decorated_function


def represents_int(s):
    # https://stackoverflow.com/questions/1265665/how-can-i-check-if-a-string-represents-an-int-without-using-try-except

    try:
        int(s)
        return True
    except ValueError:
        return False


def sanitize_string(s):
    # Replace spaces with hyphens
    s = re.sub(' ', '-', s)

    # Remove non alphanumeric characters
    s = re.sub('[^0-9a-zA-Z\-]+', '', s)

    # If all characters are removed somehow convert string to "Empty"
    s = s if len(s) > 0 else "Empty"
    return s
