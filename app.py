import os
import base64
import time
import re
import redis

from os import environ

from flask import Flask, flash, jsonify, redirect, render_template, request, session
from flask_session import Session
from flask_sqlalchemy import SQLAlchemy
from tempfile import mkdtemp
from werkzeug.exceptions import default_exceptions, HTTPException, InternalServerError
from werkzeug.security import check_password_hash, generate_password_hash
from datetime import datetime

from helpers import apology, login_required, represents_int, sanitize_string
from ml import train_model, predict

# Configure application
app = Flask(__name__)

# Ensure templates are auto-reloaded
app.config["TEMPLATES_AUTO_RELOAD"] = True

# Configure Flask SQL Alchemy
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ['DATABASE_URL']
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

# Ensure responses aren't cached
if os.getenv("FLASK_ENV") == "development":
    @app.after_request
    def after_request(response):
        response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
        response.headers["Expires"] = 0
        response.headers["Pragma"] = "no-cache"
        return response


# Configure session to use redis object cache
app.config["SESSION_PERMANENT"] = False
app.config["SECRET_KEY"] = "b'~[\xca\xfe\x1c83p4>~\xbb'"
app.config["SESSION_TYPE"] = "redis"
app.config["SESSION_REDIS"] = redis.from_url(
    "redis://:hIhrmIYM6EEqmhugV1YFbXn2X3ZmMKRh@redis-15143.c10.us-east-1-3.ec2.cloud.redislabs.com:15143")

Session(app)

# Configure SQLAlchemy to use postgresql database
db = SQLAlchemy(app)


class User(db.Model):
    # Define mapping for User
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String, unique=True, nullable=False)
    password = db.Column(db.String, unique=True, nullable=False)

    def __repr__(self):
        return "<User(id='%i', username='%s', password='%s')>" % (self.id, self.username, self.password)


class Model(db.Model):
    # Define mapping for Model
    __tablename__ = 'models'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=False, nullable=False)
    userid = db.Column(db.Integer, unique=False, nullable=False)
    created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return "<User(name='%s', userid='%s', created='%s')>" % (self.name, self.userid, self.created)


@app.route("/")
def index():
    """Show homepage"""

    return render_template("index.html")


@app.route("/about")
def about():
    """Show about page"""

    return render_template("about.html")


@app.route("/check", methods=["GET"])
def check():
    """Return true if username available, else false, in JSON format"""

    # Get username from GET request
    username = request.args.get("username")

    # Get all existing users
    users = User.query.all()

    # Function that checks if input is username in existing usernames
    def isUsernameAvailable(username):
        for user in users:
            if username == user.username:
                return False
        return True

    # Check if length of username is greater than 1 and doesn't already exist
    if len(username) > 1 and isUsernameAvailable(username):
        return jsonify(True)

    return jsonify(False)


@app.route("/create-model", methods=["GET", "POST"])
@login_required
def create_model():
    """Create model"""

    # User reached route via POST
    if request.method == "POST":

        model = request.form.get("model")
        features_count = request.form.get("featuresCount")

        # Check if model or features count is empty
        if not model or not features_count:
            return apology("model and features may not be empty")

        # Check if model size is between 2 and 15
        if len(model) < 2 or len(model) > 15:
            return apology("model name must be between 2 and 15 characters long")

        # Check if features count is integer
        if not represents_int(features_count):
            return apology("features number is not an integer")

        # Typecast features count to integer
        features_count = int(features_count)

        # Check if features is between 2 and 5
        if features_count < 2 or features_count > 5:
            return apology("number of features must be between 2 and 5")

        # Loop through potential features
        features = []
        for i in range(features_count):

            # Check if feature exists
            if not request.form.get(f"feature{i}"):
                return apology("features may not be empty")

            # Sanitize feature string
            feature = sanitize_string(request.form.get(f"feature{i}"))

            # Check if feature is unique
            if feature in features:
                return apology("features must be unique")

            # Add feature to list
            features.append(feature)

        # Sanitize model string
        model = sanitize_string(model)

        # Query database for model for current user
        models = Model.query.filter_by(
            name=model, userid=session["user_id"]).first()

        # Check if modelname already exists
        if models:
            return apology("model already exists")

        # Create model folder
        try:
            os.makedirs(f"models/{session['user_id']}/{model}")
        except FileExistsError:
            return apology("%s directory already exists" % model)

        # Add row to database with model
        new_model = Model(name=model, userid=session["user_id"])
        db.session.add(new_model)
        db.session.commit()

        # Create feature folders
        for feature in features:
            try:
                os.makedirs(f"models/{session['user_id']}/{model}/{feature}")
            except FileExistsError:
                return apology("%s directory already exists" % model)

        return redirect("/models")

    # User reached route via GET
    else:
        return render_template("create-model.html")


@app.route("/feed-model", methods=["POST"])
@login_required
def feed_model():
    """Feed model"""

    print("test")
    model = request.form.get('model')

    # Check if model is empty
    if not model:
        return apology("model may not be empty")

    # Check if model exists in database
    if not check_if_model_exists(model):
        return apology("this model does not exist")

    # Get features (subfolders) from model (directory)
    features = get_features_from_model(model)

    # Check if directory exists
    if not features:
        return apology("features directory does not exist")

    # Loop through features and check if form was filled with this feature
    def checkIfAllFeaturesExists():
        for feature in features:
            if not request.form.get(feature):
                return False
        return True

    # Check if features from form all exist (this is important because dataset needs to be equally distributed)
    if checkIfAllFeaturesExists():

        for feature in features:
            # Format imgdata code correctly
            imgdata = request.form.get(feature).split(",")[1]

            # Convert bas64 data to JPEG data
            try:
                imgdata = base64.b64decode(imgdata)
            except:
                return apology("invalid image data")

            # Create file with unique stamp and place in correct directory
            filename = f"./models/{session['user_id']}/{model}/{feature}/{time.time()}.jpg"
            with open(filename, 'wb') as f:
                f.write(imgdata)

    else:
        return apology("not all features exist for this model")

    # Get number of feeds from model
    feed_count = get_feed_count_from_model(model, features)

    flash('Model was successfully fed')
    return render_template("feed-model.html", model=model, features=features, feed_count=feed_count)


@app.route("/help")
def help():
    """Show help page"""

    return render_template("help.html")


@app.route("/login", methods=["GET", "POST"])
def login():
    """Log user in"""

    # Forget any user_id
    session.clear()

    # User reached route via POST (as by submitting a form via POST)
    if request.method == "POST":

        username = request.form.get("username")
        password = request.form.get("password")

        # Ensure username was submitted
        if not username:
            return apology("must provide username", 403)

        # Ensure password was submitted
        elif not password:
            return apology("must provide password", 403)

        # Query database for user
        user = User.query.filter_by(username=username).first()

        # Ensure username exists and password is correct
        if not user or not check_password_hash(user.password, password):
            return apology("invalid username and/or password", 403)

        # Remember which user has logged in
        session["user_id"] = user.id

        # Redirect user to home page
        flash('You were successfully logged in')
        return redirect("/models")

    # User reached route via GET (as by clicking a link or via redirect)
    else:
        return render_template("login.html")


@app.route("/logout")
def logout():
    """Log user out"""

    # Forget any user_id
    session.clear()

    # Redirect user to login form
    flash('You were successfully logged out')
    return redirect("/")


@app.route("/models", methods=["GET", "POST"])
@login_required
def models():
    """Models dashboard"""

    # Query database for models for current user
    models = Model.query.filter_by(userid=session["user_id"])

    # User reached route via POST
    if request.method == "POST":

        model = request.form.get("model")

        # Check if model is empty
        if not model:
            return apology("model may not be empty")

        # Check if model exists in database
        if not check_if_model_exists(model):
            return apology("this model does not exist")

        # Get features (subfolders) from model (directory)
        features = get_features_from_model(model)

        # Check if directory exists
        if not features:
            return apology("features directory does not exist")

        # Get number of feeds from model
        feed_count = get_feed_count_from_model(model, features)

        # Feed model
        if request.form.get("action") == "feed":
            return render_template("feed-model.html", model=model, features=features, feed_count=feed_count)

        # Train model
        if request.form.get("action") == "train":

            # Check if model is fed at all
            if feed_count <= 0:
                return apology("the model needs to be fed before it can be trained")

            # Train and check if training was succesful
            if not train_model(model, session["user_id"]):
                return apology("training failed")

            flash('Model was trained successfully')
            return redirect("/models")

        # Test model
        if request.form.get("action") == "test":
            return render_template("test-model.html", model=model, features=features)

    # User reached route via GET
    else:
        return render_template("models.html", models=models)


@app.route("/test-model", methods=["POST"])
@login_required
def test_model():
    """Test model"""

    model = request.form.get('model')
    image = request.form.get('image')

    # Check if model is empty
    if not model:
        return apology("model may not be empty")

    # Check if model exists in database
    if not check_if_model_exists(model):
        return apology("this model does not exist")

    # Check if image is not empty
    if not image:
        return apology("image is empty")

    # Get features (subfolders) from model (directory)
    features = get_features_from_model(model)

    # Check if directory exists
    if not features:
        return apology("features directory does not exist")

    # Convert base64 to image data
    imgdata = image.split(",")[1]
    try:
        imgdata = base64.b64decode(imgdata)
    except:
        return apology("invalid image data")

    # Predict with model and image data
    prediction = predict(model, imgdata, session["user_id"])

    # Check if prediction was succesful
    if not prediction:
        return apology("prediction failed. did you train m3 already?")

    return render_template("test-model.html", model=model, prediction=prediction, features=features)


@app.route("/register", methods=["GET", "POST"])
def register():
    """Register user"""

    # User reached route via POST
    if request.method == "POST":
        username = request.form.get("username")
        password = request.form.get("password")
        confirmation = request.form.get("confirmation")

        # Query database for user
        user = User.query.filter_by(username=username).first()

        # Check if username is empty or already exists
        if not username or user:
            return apology("username is empty or already exists")

        # Check if passwords are empty or do not match
        if not password or not confirmation or password != confirmation:
            return apology("password is empty or is not the same")

        # Add row to database with user information
        new_user = User(username=username,
                        password=generate_password_hash(password))
        db.session.add(new_user)
        db.session.commit()

        # Query database for userid
        userid = User.query.filter_by(username=username).first().id

        # Create user folder based on id
        try:
            os.makedirs(f"models/{userid}")
        except FileExistsError:
            return apology("%s directory already exists" % userid)

        # Redirect user to home page
        flash('You were registered successfully')
        return redirect("/")

    # User reached route via GET
    else:
        return render_template("register.html")


def errorhandler(e):
    """Handle error"""
    if not isinstance(e, HTTPException):
        e = InternalServerError()
    return apology(e.name, e.code)


# Listen for errors
for code in default_exceptions:
    app.errorhandler(code)(errorhandler)


def check_if_model_exists(model):
    # Query database for models for current user
    models = Model.query.filter_by(userid=session["user_id"])

    # Loop through models from database and check if it matches the model string
    for m in models:
        if m.name == model:
            return True

    return False


def get_features_from_model(model):
    dir = f"./models/{session['user_id']}/{model}"

    # Loop through directory and fill features list with non-hidden subdirectories
    try:
        features = next(os.walk(dir))[1]
    except StopIteration:
        return False

    return features


def get_feed_count_from_model(model, features):

    dir = f"./models/{session['user_id']}/{model}/{features[0]}"

    # Loop through directory and get length of all files in first feature of model
    try:
        feed_count = len(next(os.walk(dir))[2])
    except StopIteration:
        return -1

    return feed_count
