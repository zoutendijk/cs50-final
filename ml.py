# This code is largely based on a youtube series by sentdex: https://www.youtube.com/watch?v=wQ8BIBpya2k&t=4s

import tensorflow as tf
import numpy as np
import os
import cv2
import random
import time

from helpers import apology

IMG_SIZE = 50


def create_training_data(dir):
    training_data = []
    features = []
    labels = []

    # Get subfolders (non-hidden) from directory and put them in list as strings
    try:
        subfolders = next(os.walk(dir))[1]
    except StopIteration:
        return False

    # Loop through subfolders
    for subfolder in subfolders:

        # Create path with directory name + category name
        path = os.path.join(dir, subfolder)

        # Create classifier based on the index of the category
        class_num = subfolders.index(subfolder)

        # Loop through all images in current subfolder
        for img in os.listdir(path):

            # Test image for errors
            try:

                # Make an array out of the current image
                img_array = cv2.imread(os.path.join(
                    path, img), cv2.IMREAD_GRAYSCALE)
                new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))

                # Append image array and classifier to traing_data list
                training_data.append([new_array, class_num])

            # Pass if image is corrupted or otherwise throws an error
            except Exception as e:
                pass

    # Randomly shuffle training data
    random.shuffle(training_data)

    # Create features and labels lists
    for feature, label in training_data:
        features.append(feature)
        labels.append(label)

    # Convert lists to NumPy arrays
    features = np.array(features).reshape(-1, IMG_SIZE, IMG_SIZE)
    labels = np.array(labels)

    # Return the features, labels and number of categories as a tupple
    return (features, labels, len(subfolders))


def train_model(model, userid):

    dir_name = f"./models/{userid}/{model}"
    file_name = f"./models/{userid}/{model}/{model}.model"

    # Get training data from model
    training_data = create_training_data(dir_name)

    # Check if directories exist
    if not training_data:
        return False

    # Build data from data tuple
    (features, labels, nResults) = training_data

    # Check if training data exists
    if len(features) == 0:
        return False

    # Normalize training data
    features = tf.keras.utils.normalize(features, axis=1)

    # Define neural network layers
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Flatten(input_shape=(IMG_SIZE, IMG_SIZE)))
    model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(128, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(nResults, activation=tf.nn.softmax))
    model.compile(optimizer='adam',
                  loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    model.fit(features, labels, epochs=64)

    # Save model to file
    model.save(file_name)

    return True


def predict(model, imgdata, userid):
    dir_name = f"models/{userid}/{model}"
    file_name = f"models/{userid}/{model}/{model}.model"

    # Load model
    try:
        new_model = tf.keras.models.load_model(file_name)
    except:
        return False

    # Create tmp file for image
    tmp = f'{time.time()}.jpg'
    with open(tmp, 'wb') as f:
        f.write(imgdata)

    # Preprocess image to fit model
    img_array = cv2.imread(tmp, cv2.IMREAD_GRAYSCALE)
    img_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
    test_data = np.array(img_array).reshape(-1, IMG_SIZE, IMG_SIZE)

    # Make prediction based on model and input data
    predictions = new_model.predict(test_data)

    # Remove tmp image
    os.remove(tmp)

    # Create legend based on model directory
    try:
        legend = next(os.walk(dir_name))[1]
    except StopIteration:
        return False

    # Return prediction
    return legend[np.argmax(predictions[0])]
